<?php

require_once "../config.php";

if (isset($_POST['submit'])) {

    $query = "INSERT INTO book (Title, Author, Series, HaveBook,NumInSeries) VALUES ('" . addslashes($_POST["title"]) . "','" . $_POST["author"] . "','" . addslashes($_POST["series"]) . "',
              '" . $_POST["havebook"] . "', '".$_POST["num"]."')";
    mysqli_query($connect, $query);

    header("Location: index.php?status=1");
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link rel="stylesheet" href="../css/foundation.css"/>
    <link rel="stylesheet" href="../css/style.css"/>
    <script src="../js/jquery-2.1.4.min.js"></script>
    <script src="../js/foundation.js"></script>
</head>
<body>
<form method="post" class="small-12">
    <div>
        <span class="label-form form-input">Title</span>
        <input type="text" name="title" class="input-group-field form-input">
    </div>
    <div>
        <span class="label-form form-input">Author</span>
        <input type="text" name="author" class="input-group-field form-input">
    </div>
    <div>
        <span class="label-form form-input">Series</span>
        <input type="text" name="series" class="input-group-field form-input">
    </div>
    <div>
        <span class="label-form form-input">Have Book?</span>
        <select name="havebook" class="input-group-field">
            <option value="0" class="form-input">No</option>
            <option value="1" class="form-input">Yes</option>
        </select>
    </div>
    <div>
        <span class="label-form form-input">Number In Series</span>
        <input type="text" name="num" class="input-group-field">
    </div>
    <div>
            <input type="submit" class="success expanded button add" name="submit" value="Create">
            <a href="index.php" class="alert expanded button cancel">Cancel</a>
    </div>
</form>
</body>
</html>
