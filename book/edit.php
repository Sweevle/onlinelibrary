<?php
require_once "../config.php";

$id = $_GET['id'];

$query = "SELECT Title, Author,Series,HaveBook,NumInSeries FROM book WHERE id = '" . $id . "'";
$result = mysqli_query($connect, $query);
$details = mysqli_fetch_object($result);

if (isset($_POST['submit'])) {

    $query = "UPDATE book SET Title = '" . addslashes($_POST["title"]) . "', Author = '" . $_POST["author"] . "', Series ='" . addslashes($_POST["series"]) . "',
                HaveBook = '" . $_POST["havebook"] . "', NumInSeries='". $_POST["num"]. "' WHERE id ='" . $id . "'";
    mysqli_query($connect, $query);

    header("Location: index.php?status=2");
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link rel="stylesheet" href="../css/foundation.css"/>
    <link rel="stylesheet" href="../css/style.css"/>
</head>
<body>
<form method="post" class="small-12">
    <div>
        <span class="label-form form-input">Title</span>
        <input type="text" name="title" class="input-group-field" value="<?= stripslashes($details->Title) ?>">
    </div>
    <div>
        <span class="label-form form-input">Author</span>
        <input type="text" name="author" class="input-group-field" value="<?= stripslashes($details->Author) ?>">
    </div>
    <div>
        <span class="label-form form-input">Series</span>
        <input type="text" name="series" class="input-group-field" value="<?= stripslashes($details->Series) ?>">
    </div>
    <div>
        <span class="label-form form-input">Have Book?</span>
        <select name="havebook" class="input-group-field">
            <?php if (stripslashes($details->HaveBook) == 1) {?>
                <option value="1">Yes</option>
                <option value="0">No</option>
            <?php } else {?>
                <option value="0">No</option>
                <option value="1">Yes</option>
            <?php } ?>
        </select>
    </div>
    <div>
        <span class="label-form form-input">Number In Series</span>
        <input type="text" name="num" class="input-group-field" value="<?= stripslashes($details->NumInSeries) ?>">
    </div>
    <div>
        <input type="submit" class="secondary expanded button add" name="submit" value="Submit">
        <a href="index.php" class="alert expanded button cancel">Cancel</a>
    </div>
</form>
</body>
</html>