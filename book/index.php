<?php
require_once "../config.php";

$query = "SELECT * FROM book ORDER BY Author, Series, NumInSeries ASC";

$results = mysqli_query($connect, $query);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link rel="stylesheet" href="../css/foundation.css"/>
    <link rel="stylesheet" href="../css/style.css"/>
    <script src="../js/jquery-2.1.4.min.js"></script>
    <script src="../js/foundation.js"></script>
</head>
<body>

<div class="small-12">
    <a href="../" class="expanded button return">< Back</a>
    <a href="create.php" class="expanded success button add">Create</a>
</div>

<?php if (isset($_GET["status"])) {
    if ($_GET["status"] == 1) {
        echo "<h2>Je boek is toegevoegd</h2>";
    } elseif ($_GET["status"] == 2) {
        echo "<h2>Je boek is bewerkt</h2>";
    }
}; ?>
<div class="small-12 outer-table-wrapper">
    <table class="hover responsive list expanded">

        <thead>
        <tr>
            <th>Title</th>
            <th>Author</th>
            <th>Series</th>
            <th>Have Book?</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($results as $item) {
            if ($item['HaveBook'] == 1) {
                $item['HaveBook'] = "Yes";
            } else {
                $item['HaveBook'] = "No";
            }
            ?>
            <tr>
                <td><?= $item['Title'] ?></td>
                <td><?= stripslashes($item['Author']) ?></td>
                <td><?= $item['Series'] ?></td>
                <td><?= stripslashes($item['HaveBook']) ?></td>
                <td class="button-group edit">
                    <a href="edit.php?id=<?= $item['id'] ?>" class="success button">Edit</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(document).foundation();
</script>
</body>
</html>
