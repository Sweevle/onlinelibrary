<?php

require_once "../config.php";

if (isset($_POST['submit'])) {

    $query = "INSERT INTO game (Title, Platform, Series, HaveGame, NumInSeries) VALUES ('" . addslashes($_POST["title"]) . "','" . addslashes($_POST["platform"]) . "','" . addslashes($_POST["series"]) . "',
              '" . addslashes($_POST["havegame"]) . "', '".addslashes($_POST["num"])."')";
    mysqli_query($connect, $query);

    header("index.php?status=1");
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link rel="stylesheet" href="../css/foundation.css"/>
    <link rel="stylesheet" href="../css/style.css"/>
    <script src="../js/jquery-2.1.4.min.js"></script>
    <script src="../js/foundation.js"></script>
</head>
<body>
<form method="post">
    <div>
        <span class="label-form form-input">Title</span>
        <input type="text" name="title" class="input-group-field form-input">
    </div>
    <div>
        <span class="label-form form-input">Platform</span>
        <select name="platform" class="input-group-field">
                <option value="PC" class="input-group-field form-input">PC</option>
                <option value="3DS" class="input-group-field form-input">3DS</option>
                <option value="NDS" class="input-group-field form-input">NDS</option>
                <option value="GBA" class="input-group-field form-input">GBA</option>
                <option value="GBC" class="input-group-field form-input">GBC</option>
                <option value="Wii U" class="input-group-field form-input">Wii U</option>
                <option value="Wii" class="input-group-field form-input">Wii</option>
                <option value="GC" class="input-group-field form-input">GC</option>
                <option value="N64" class="input-group-field form-input">N64</option>
                <option value="SNES" class="input-group-field form-input">SNES</option>
                <option value="NES" class="input-group-field form-input">NES</option>
                <option value="PS4" class="input-group-field form-input">PS4</option>
                <option value="PS3" class="input-group-field form-input">PS3</option>
                <option value="PS2" class="input-group-field form-input">PS2</option>
                <option value="PS1" class="input-group-field form-input">PS1</option>
                <option value="PSP" class="input-group-field form-input">PSP</option>
                <option value="PSV" class="input-group-field form-input">PSV</option>
        </select>
    </div>
    <div>
        <span class="label-form form-input">Series</span>
        <input type="text" name="series" class="input-group-field form-input">
    </div>
    <div>
        <span class="label-form form-input">Have Game?</span>
        <select name="havegame" class="input-group-field">
            <option value="0" class="form-input">No</option>
            <option value="1" class="form-input">Yes</option>
        </select>
    </div>
    <div>
        <span class="label-form form-input">Number In Series</span>
        <input type="text" name="num" class="input-group-field">
    </div>
    <div>
        <input type="submit" class="success expanded button add" name="submit" value="Create">
        <a href="index.php" class="alert expanded button cancel">Cancel</a>
    </div>
</form>
</body>
</html>
