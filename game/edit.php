<?php
require_once "../config.php";

$id = $_GET['id'];

$query = "SELECT Title, Platform ,Series,HaveGame,NumInSeries FROM game WHERE ID = '" . $id . "'";
$result = mysqli_query($connect, $query);
$details = mysqli_fetch_object($result);

if (isset($_POST['submit'])) {

    $query = "UPDATE game SET Title = '" . addslashes($_POST["title"]) . "', Platform = '" . addslashes($_POST["platform"]) . "',
              Series = '" . addslashes($_POST["series"]) . "', HaveGame = '" . addslashes($_POST["havegame"]) . "',
              NumInSeries= '" . addslashes($_POST["num"]) . "' WHERE ID = '" . $id . "'";

    mysqli_query($connect, $query);

    header("index.php?status=2");
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link rel="stylesheet" href="../css/foundation.css"/>
    <link rel="stylesheet" href="../css/style.css"/>
</head>
<body>
<form method="post">
    <div>
        <span class="label-form form-input">Title</span>
        <input type="text" name="title" class="input-group-field" value="<?= stripslashes($details->Title) ?>">
    </div>
    <div>
        <span class="label-form form-input">Platform</span>
        <select name="platform" class="input-group-field">
            <option value="<?= stripslashes($details->Platform) ?>" class="form-input"><?= stripslashes($details->Platform) ?></option>
            <option value="PC" class="form-input">PC</option>
            <option value="3DS" class="form-input">3DS</option>
            <option value="NDS" class="form-input">NDS</option>
            <option value="GBA" class="form-input">GBA</option>
            <option value="GBC" class="form-input">GBC</option>
            <option value="Wii U" class="form-input">Wii U</option>
            <option value="Wii" class="form-input">Wii</option>
            <option value="GC" class="form-input">GC</option>
            <option value="N64" class="form-input">N64</option>
            <option value="SNES" class="form-input">SNES</option>
            <option value="NES" class="form-input">NES</option>
            <option value="PS4" class="form-input">PS4</option>
            <option value="PS3" class="form-input">PS3</option>
            <option value="PS2" class="form-input">PS2</option>
            <option value="PS1" class="form-input">PS1</option>
            <option value="PSP" class="form-input">PSP</option>
            <option value="PSV" class="form-input">PSV</option>
        </select>
        <div>
            <span class="label-form form-input">Series</span>
            <input type="text" name="series" class="input-group-field" value="<?= stripslashes($details->Series) ?>">
        </div>
        <div>
            <span class="label-form form-input">Have Game?</span>
            <select name="havegame" class="input-group-field">
                <?php if (stripslashes($details->HaveGame) == 1) {?>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                <?php } else {?>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                <?php } ?>
            </select>
        </div>
        <div>
            <span class="label-form form-input">Number In Series</span>
            <input type="text" name="num" class="input-group-field" value="<?= stripslashes($details->NumInSeries) ?>">
        </div>
        <div>
            <input type="submit" class="secondary expanded button add" name="submit" value="Submit">
            <a href="index.php" class="alert expanded button cancel">Cancel</a>
        </div>
</form>

</body>
</html>