<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link rel="stylesheet" href="css/foundation.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/foundation.js"></script>
</head>
<body>
<div>
    <div class="title-bar">
        <div class="title-bar-left">
            <span class="title-bar-title"><a href="index.php" class="title"> Online Library</a></span>
        </div>
    </div>

    <a href="book/index.php" class=" expanded button menu-item">Books</a>
    <a href="game/index.php" class=" expanded button menu-item">Games</a>
</div>
</body>
</html>
